<?php

declare(strict_types=1);

namespace Modulith\ArchCheck\Test\Fixtures\ComponentA;

final class ClassAWithoutDependencies
{
}
