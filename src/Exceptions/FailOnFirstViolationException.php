<?php

declare(strict_types=1);

namespace Modulith\ArchCheck\Exceptions;

class FailOnFirstViolationException extends \Exception
{
}
